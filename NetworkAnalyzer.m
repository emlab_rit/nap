classdef NetworkAnalyzer < handle
    properties
        name
        VISA_conn
        GPIBAdapter
        RsrcName
    end
    
    methods
        function obj=NetworkAnalyzer(GPIBAdapter,ResourceName)
            if(ischar(GPIBAdapter) && ischar(ResourceName))
                obj.GPIBAdapter=GPIBAdapter;
                obj.RsrcName=ResourceName;
            else
                % Default
                obj.GPIBAdapter='ni';
                obj.RsrcName='GPIB0::16::0::INSTR';
            end
        end
        
        function status=connect(obj)
            try
                % Find the VISA-GPIB object for NA at GPIB0 address 16 (default for NA).
                obj.VISA_conn = instrfind('Type', 'visa-gpib', 'RsrcName', obj.RsrcName, 'Tag', '');
                % Create the VISA-GPIB object if it does not exist
                % otherwise use the object that was found.
                if isempty(obj.VISA_conn)
                    obj.VISA_conn = visa(obj.GPIBAdapter, obj.RsrcName);
                else
                    fclose(obj.VISA_conn);
                    obj.VISA_conn = obj.VISA_conn(1);
                end
                
                % Increase the buffer size (Provide Sufficient size)
                set(obj.VISA_conn, 'InputBufferSize', 524288);
                set(obj.VISA_conn, 'OutputBufferSize', 512);
                set(obj.VISA_conn, 'EOIMode', 'on');
                set(obj.VISA_conn, 'EOSCharCode', 'LF');
%                 set(obj.VISA_conn, 'EOSMode', 'none');
%                 set(obj.VISA_conn, 'EOIMode', 'off');
                set(obj.VISA_conn, 'EOSMode', 'read');
                set(obj.VISA_conn, 'Timeout', 10);   % Timeout: 1 sec
                
                % Connect to Network Analyzer
                fopen(obj.VISA_conn);
                status=0;
            catch
                status=-1; %#ok<NASGU>
                disp('Something wrong with the VISA library drivers!');
                disp('Can not open the connection to Network Analyzer');
            end
            % Get Identification of Network Analyzer
            obj.name=obj.query('*IDN?;');
            if obj.name<0
                disp('Network Analyzer not connected! Please check the connection');
                status=-10;
            else
                status=0;
            end
        end
        
        function status=disconnect(obj)
            try
                fclose(obj.VISA_conn);
                status=0;
            catch
                status=-1;
            end
        end
        
        function status=send(obj,cmd)
            try
                % Pause added to reduce write errors(ot timeout)
                pause(0.1);
                fprintf(obj.VISA_conn, '%s\n', cmd);
                pause(0.1);
                status=0;
            catch
                status=-1;
            end
        end
        
        function [data,count,msg]=recv(obj)
            try
                [data,count,msg]= fscanf(obj.VISA_conn);
            catch
                data=-1;
            end
        end
        
        function [data,msg]=query(obj,cmd)
           s=obj.send(cmd);
           if s==0
               [data,count,msg]=obj.recv();
               if isempty(msg)==0
                  data=-3 ;
               end
           else
               % Error
               msg='error: Cannot send';
               data=-2;
           end
        end
        
        % FORM5 data size = 2bytes(header- '#A')+2bytes(integer- no of data
        % to follow - N)+N bytes of data(each point(POIN?) has 2 data value little endian 32bit float)
        function data=read_form5_data(obj, number_of_points)
            try
                eoi_status=obj.VISA_conn.EOIMode;
                eos_status=obj.VISA_conn.EOSMode;
                set(obj.VISA_conn, 'EOSMode', 'none');
                set(obj.VISA_conn, 'EOIMode', 'off');

                num_data_to_read=(number_of_points*2*4);
                header= fread(obj.VISA_conn,2,'uchar');
                num_bytes = fread(obj.VISA_conn,1,'int16');

                data = fread(obj.VISA_conn,[2 num_data_to_read/8],'float');
                
                set(obj.VISA_conn, 'EOIMode', eoi_status);
                set(obj.VISA_conn, 'EOSMode', eos_status);
            catch
                data=[];
            end
        end
        
        function status=check_conn(obj)
            if obj.query('*IDN?;')<0
                % Error
                status=-1;
            else
                status=0;
            end
        end
        
        function status_byte=get_status_byte(obj)
            status_byte=obj.query('OUTPSTAT;')
            if status_byte<0
                status_byte=[]
            end
        end
        
        function err_msg=get_err_msg(obj)
            err_msg=obj.query('OUTPERRO;')
            if err_msg<0
                err_msg=[]
            end
        end
        
        function esr=get_esr_byte(obj)
            esr_byte=obj.query('ESR?;')
            if esr_byte<0
                esr_byte=[]
            end
        end
        
        function esb=get_esb_byte(obj)
            esb_byte=obj.query('ESB?;')
            if esb_byte<0
                esb_byte=[]
            end
        end
        

         function clear_buffers(obj)
            clrdevice(obj.VISA_conn)
         end
    end
        
end