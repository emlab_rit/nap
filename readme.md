# Network Analyzer Plotter

## Requirement
* GPIB card - To connect network analyzer and PC
* Matlab with keysight io support package

Note: Install drivers for GPIB-PCI card. For Agilent cards, install Keysight/Agilent IO lib suite. For NI cards, install NI488.2 version 16.

## Downloading code
```
git clone --recursive https://rounaksingh17@bitbucket.org/emlab_rit/nap.git
```

For NA 8720B, commands should be followed by '\r\n'(EOS). For all other NA, commands should be followed by '\n'

