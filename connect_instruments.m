function handles=connect_instruments(handles)
    instrreset;
%     handles.objNA=[];
    
    try
    %establish connection with NA
    if(~isempty(handles.RsrcName) && ischar(handles.RsrcName))
        handles.objNA = NetworkAnalyzer(handles.GPIBAdapter, handles.RsrcName);
        if(handles.objNA.connect()==0 && ~isempty(handles.objNA.name))
            % Connected to NA
            handles.objNA.clear_buffers();
        else
%           errordlg('No Connection between Computer and Network Analyer!');
            fprintf('No Connection between Computer and Network Analyer!\n');
%             return;
        end
    else
        disp('Wrong GPIB port info');
        
%         return;
    end

    catch
%         errordlg('No Connection between Computer and Network Analyer!');
        fprintf('No Connection between Computer and Network Analyer!\n');
%         return
    end
