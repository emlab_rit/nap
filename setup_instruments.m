% Setup the instrument addresses
% if the settings.mat exist then load the variables 
% otherwise call choosedialog() to get the address of instruments
%
% Written by Rounak Singh Narde
%

function handles=setup_instruments(handles)

if(exist([handles.settings_path handles.settings_filename],'file')==0)
    % Create the directory if not exist
    if(exist(handles.settings_path)==7)

    else
        mkdir(handles.settings_path);
    end
    [handles.GPIBAdapter,handles.RsrcName]=choosedialog();
    temp_GPIBAdapter=handles.GPIBAdapter;
    temp_RsrcName=handles.RsrcName;

    if(~isempty(temp_GPIBAdapter) && ~isempty(temp_RsrcName))
        disp('settings file saved');
        save([handles.settings_path handles.settings_filename],'temp_GPIBAdapter','temp_RsrcName');
    else
        % Do if wrong information filled
        disp('settings file Not saved');
        errordlg('Wrong Address or Turned off. Please check!','Error')
    end
elseif(exist([handles.settings_path handles.settings_filename],'file')==2)
    load([handles.settings_path handles.settings_filename]);
%     if(exist(temp_GPIBAdapter) && exist(temp_RsrcName))
        if(~isempty(temp_GPIBAdapter) && ~isempty(temp_RsrcName))
            handles.GPIBAdapter=temp_GPIBAdapter;
            handles.RsrcName=temp_RsrcName;
        else
            % one of the variable empty
            delete([handles.settings_path handles.settings_filename]);
        end
%     else
%         % one of the variable not exist
%         delete([handles.settings_path handles.settings_filename]);
%     end
end

clear temp_GPIBAdapter,clear temp_RsrcName;
