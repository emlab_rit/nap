% GUI for selecting GPIO board and COM port for Halfstep
% For only one GPIB board, 
% 
% Written by Rounak Singh Narde

function [GPIBAdapter,RsrcName]= choosedialog
    instrreset;
    GPIBAdapter=[];
    RsrcName=[];

    % Below variables should be error to make sure that the connections are
    % OK. Positive values means connection error.
    NA_OK=2;
    
    d = dialog('Position',[300 300 300 300],'Name','Settings');
    txt = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[50 260 210 40],...
           'String','Select GPIB drivers');
    txt = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[10 240 280 40],...
           'String','Remember: Click on pop-up menu & click on your choice');
    
    popup_drivers = uicontrol('Parent',d,...
           'Style','popup',...
           'Position',[100 240 100 25],...
           'String','Click me',...
           'Callback',@popup_drivers_callback);
       
    popup_instrument = uicontrol('Parent',d,...
           'Style','popup',...
           'Position',[100 210 100 25],...
           'String',{'No instrument'},...
           'Callback',@popup_instrument_callback);
    
    txt_device_name = uicontrol('Parent',d,...
           'Style','text',...
           'Position',[50 80 210 40],...
           'String','No device connected');
    
   txt_NA8720B_name = uicontrol('Parent',d,...
       'Style','edit',...
       'Position',[70 130 150 20],...
       'String','GPIB0::16::INSTR');
   txt_NA8720B_name.Enable='off';
   
   cbx_if_NA8720B = uicontrol('Parent',d,'Style','checkbox', ...
       'String','For HP 8720B', ...
       'Value',0,'Position',[70 160 210 40],     ...
       'Callback',@checkBoxCallback);
                    
    btn = uicontrol('Parent',d,...
           'Position',[170 20 100 25],...
           'String','Save & Exit',...
           'Callback',@btn_callback);
    btn.Enable='off';
                 
    verify = uicontrol('Parent',d,...
           'Position',[30 20 100 25],...
           'String','Verify',...
           'Callback',@verify_callback);
    

      % Get details about the instruments GPIB
      gpibHWInfo = instrhwinfo('gpib');
      if(~isempty(gpibHWInfo.InstalledAdaptors))
        popup_drivers.String=gpibHWInfo.InstalledAdaptors;
        GPIBAdapter=gpibHWInfo.InstalledAdaptors(1);
      else
        popup_drivers.String={'No GPIB'};
        popup_drivers.Value=1;
      end
      
   
    % Wait for d to close before running to completion
    uiwait(d);
   
   function popup_drivers_callback(popup_drivers,event)
      idx = popup_drivers.Value;
      popup_items = popup_drivers.String;
      gpio_selected_drivers = char(popup_items(idx,:));
      instrreset
      driver_details=instrhwinfo('gpib',gpio_selected_drivers);
      if(~isempty(driver_details.ObjectConstructorName))
        popup_instrument.String=driver_details.ObjectConstructorName;            
      else
        popup_instrument.String={'No instrument'};
        popup_instrument.Value=1;
        txt_device_name.String='No device connected';
      end
      RsrcName=[];
   end
   
   
    function popup_instrument_callback(popup_instrument,event)
          idx = popup_instrument.Value;
          popup_items = popup_instrument.String;
          instrumentID = char(popup_items(idx,:));
          
          [b,count]=sscanf(instrumentID, '%5c%s%c%d%c%d%s');
          g=char(b(1:5))';
          
          if(strcmp(g,'gpib('))
              GPIBAdapter=char(b(6:9))';
              board=b(12);
              primaryAdd=b(14);

              if(isnumeric(board) && isnumeric(primaryAdd))
                RsrcName=['GPIB' num2str(board) '::' num2str(primaryAdd) '::0::INSTR'];
                disp(RsrcName)
              else
                  disp('No Drivers details retrived');
              end
              
              % Testing connection
              idx = popup_drivers.Value;
              popup_items = popup_drivers.String;
              gpio_selected_drivers = char(popup_items(idx,:));
              % if connection successful NA_OK=0
              test_connection(gpio_selected_drivers,RsrcName)
          else
              % Error Received
              GPIBAdapter=[];
              RsrcName=[];
              popup_instrument.String={'No instrument'};
              txt_device_name.String='No device connected';
              NA_OK=3;
          end
          
    end
   
    function test_connection(GPIBAdap, Name)
      % Show that the Program is trying to connect to NA
      txt_device_name.String=['connecting'];
      verify.Enable='off';
      drawnow

      %device=gpio('ni',RsrcName);
      dev=NetworkAnalyzer(GPIBAdap,Name);
      if(dev.connect()==0 && ~isempty(dev.name))
        txt_device_name.String=['Connected to ' dev.name];
        NA_OK=0;
        GPIBAdapter=GPIBAdap;
        RsrcName=Name;
      else
        txt_device_name.String='Cannot connect';
        NA_OK=2;
      end
       % Free the control
      verify.Enable='on';
      drawnow

      % If error during setup NA, clear the GPIB variables
      % This is specially written for red x or close button
      if(NA_OK>0)
          GPIBAdapter=[];
          RsrcName=[];
      end
      % Disconnect if connected
      if(~dev.disconnect())
          clear dev;
          instrreset;
      end
    end
        
   function checkBoxCallback(cbx_if_NA8720B,event)
       if cbx_if_NA8720B.Value==0
           popup_instrument.Enable='on';
           txt_NA8720B_name.Enable='off';
       elseif cbx_if_NA8720B.Value==1
           popup_instrument.Enable='off';
           txt_NA8720B_name.Enable='on';
           verify.Enable='on';
       end
   end


   function btn_callback(btn,event)
        delete(gcf)
    end

    function verify_callback(verify,event)
        if cbx_if_NA8720B.Value==1
            %device=gpio('ni',RsrcName);
            idx = popup_drivers.Value;
            popup_items = popup_drivers.String;
            gpio_selected_drivers = char(popup_items(idx,:));
%             disp(txt_NA8720B_name.String)
            test_connection(gpio_selected_drivers,txt_NA8720B_name.String)
        end
        
        if(NA_OK==0)
            btn.Enable='on';
        else
            btn.Enable='off';
        end
    end

end

