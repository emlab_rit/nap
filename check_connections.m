% Checks connection with NA and Slotted line
% Arguments: GPIB object and serial object
% output variables: 
% 1) NA_OK = 0 if connection OK otherwise positive number
% 2) SL_OK = 0 if connection OK otherwise positive number
%
% Written by Rounak Singh Narde

function [NA_OK]=check_connections(objNA)
    warning_msg=[];
    % Checking Network Analyzer
      if(~isempty(objNA))
          if(objNA.check_conn()==0)
              % connection OK
            NA_OK=0;
          else
              % connection error
            NA_OK=1;
          end
      else
          % empty object
          NA_OK=2;
      end
      
end
