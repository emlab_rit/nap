function varargout = NAP_GUI(varargin)
% NAP_GUI MATLAB code for NAP_GUI.fig
%      NAP_GUI, by itself, creates a new NAP_GUI or raises the existing
%      singleton*.
%
%      H = NAP_GUI returns the handle to a new NAP_GUI or the handle to
%      the existing singleton*.
%
%      NAP_GUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NAP_GUI.M with the given input arguments.
%
%      NAP_GUI('Property','Value',...) creates a new NAP_GUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NAP_GUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NAP_GUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NAP_GUI

% Last Modified by GUIDE v2.5 16-Oct-2017 10:05:58

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NAP_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @NAP_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before NAP_GUI is made visible.
function NAP_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NAP_GUI (see VARARGIN)

% Choose default command line output for NAP_GUI
handles.Connection_Settings.Enable='off';
handles.Connect.Enable='off';

handles.output = hObject;
instrreset;
% addpath('network_analyzer');
% handles.objNA=NetworkAnalyzer();
handles.plotdata = [];

% Disable set axis button
%handles.btn_set_label.Enable='off';
handles.Capture_Refresh.Enable='off';
handles.Capture_Refresh.Enable='off';

handles.Connection_Settings.Enable='on';

% Plot hold on
handles.hold_plot='off';

% Plot annotation
handles.trace_color='b';
handles.trace_style='-';
handles.trace_marker='';

% X_units  X/x_units
handles.xunits=1E9;    %Default GHz

% Domain of NA
% Frequency domain: 0
% Time domain: 1
handles.domain=0;    %Default Frequency Domain

% Variables for Instruments
handles.GPIBAdapter=[];
handles.RsrcName=[];
handles.NA_OK=[];

% Choose default command line output for halfstep_gui_rev2
handles.output = hObject;
handles.closeGUI = 0; %used to determine if an error occurred during initializations

handles.settings_path='C:\Users\Public\NAP\';
handles.settings_filename='settings.mat';

% Create the directory if not exist
if(exist(handles.settings_path)==7)
    
else
    mkdir(handles.settings_path);
end

% Create the directory if not exist
if(exist([handles.settings_path 'log.txt'])==2)
    delete([handles.settings_path 'log.txt']);
end
% Start diary   
diary([handles.settings_path 'log.txt']);

% Update handles structure
guidata(hObject, handles);

% Create or load setting files.
% If setting file exist then load the variables
handles=setup_instruments(handles);
% Update handles structure
guidata(hObject, handles);

% Setup connection
handles=connect_instruments(handles);

[handles.NA_OK]=check_connections(handles.objNA);


% Connection Status
if(~isempty(handles.objNA.name) && handles.NA_OK==0)
    set(handles.text12, 'String', handles.objNA.name);
    handles.Connect.Enable='on';
    handles.Capture_Refresh.Enable='on';
    drawnow
else
    set(handles.text12, 'String', 'Error Connection');
    
end

% Update handles structure
guidata(hObject, handles);


% --- Outputs from this function are returned to the command line.
function varargout = NAP_GUI_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% axes(handles.axes1);
% cla;

% popup_sel_index = get(handles.popupmenu1, 'Value');
% switch popup_sel_index
%     case 1
%         plot(rand(5));
%     case 2
%         plot(sin(1:0.01:25.99));
%     case 3
%         bar(1:.5:10);
%     case 4
%         plot(membrane);
%     case 5
%         surf(peaks);
% end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% file = uigetfile('*.fig');
% if ~isequal(file, 0)
%     open(file);
% end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end
handles.objNA.disconnect();
delete(handles.figure1)

% --- Executes on button press in Capture_Refresh.
function Capture_Refresh_Callback(hObject, eventdata, handles)
% hObject    handle to Capture_Refresh (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% Plotting the screenshot
% First get the frequency sweep

% Disable the Capture button
handles.Capture_Refresh.Enable='off';
drawnow
    try
        start_freq=str2double(handles.objNA.query('STAR?;'));
        if isnan(start_freq)
            return
        end
        stop_freq=str2double(handles.objNA.query('STOP?;'));
        number_of_points=str2double(handles.objNA.query('POIN?;'));
        points=1:number_of_points;
        freq_sweep=start_freq+(points-1)*(stop_freq-start_freq)/(number_of_points-1);

        % handles.objNA.send('SING');
        handles.objNA.send('FORM5;');
        handles.objNA.send('OUTPFORM;');
        
        data=handles.objNA.read_form5_data(number_of_points);
        if(isempty(data))
            return
        end

        % Rearranging the data
        freq_sweep=freq_sweep';
        data=data';

        [format, format_string]=get_format(handles);
        handles.plotformat=format;
        

        if format==4 % Smith chart
            SYS_Z0=get_system_impedance(handles);
            gamma=data(:,1)+i*data(:,2);
            Z=((1+gamma)./(1-gamma))*SYS_Z0;
            s=smithchart(gamma);
        %     s.LabelVisible='off';
            handles.plotdata = [freq_sweep,gamma,Z];
            handles.btn_set_label.Enable='off';
            handles.menu_set_axis.Enable='off';
        elseif format==5 % Polar
            % Plot polar plot
        else
            handles.plotdata = [freq_sweep,data];
            trace_annotation=[handles.trace_style handles.trace_color];
            plot(handles.axes1, freq_sweep/handles.xunits,data(:,1),trace_annotation);
            grid on;
            handles.btn_set_label.Enable='on';
            handles.menu_set_axis.Enable='on';
        end
            
        if handles.domain==0    % Freq domain
            % Writing to GUI
            set(handles.text8, 'string', get_frequency_string(start_freq));
            set(handles.text9, 'string', get_frequency_string(stop_freq));
            set(handles.text10, 'string', format_string);
            set(handles.text11, 'string', number_of_points);
        else
            ref_value=str2double(handles.objNA.query('REFV?;'));
            scale_per_div_value=str2double(handles.objNA.query('SCAL?;'));
            hold(handles.axes1,'on')
            plot(handles.axes1, freq_sweep/handles.xunits,ref_value*ones(length(freq_sweep),1),'--r');
            set(handles.axes1,'YTickLabel',[]);
            yticks=[-1:9];
            yticks=ref_value+yticks*scale_per_div_value;
            set(handles.axes1,'YTick',yticks);
            ylim(handles.axes1,[min(yticks) max(yticks)]);
            hold(handles.axes1,'off')
        end
    catch
        errordlg('Error while getting data.');

    end

% Update handles structure
guidata(hObject, handles);
% Enable the Capture button
handles.Capture_Refresh.Enable='on';
drawnow

function sys_Z=get_system_impedance(handles)
sys_Z=str2double(handles.objNA.query('SETZ?;'));

% Frequency is in Hz
function s=get_frequency_string(frequency)

if frequency <1E9
    s=sprintf('%7.3f MHz', frequency/10^6);
elseif frequency >=1E9
    s=sprintf('%7.3f GHz',frequency/10^9);
else
    s='error';
end

% Get Format
function [format, format_string] = get_format(handles)
format_string = '';
if str2double(handles.objNA.query('LOGM?;'))
    format=0;
    format_string = 'Magnitude(dB)';
elseif str2double(handles.objNA.query('LINM?;'))
    format=1;
    format_string = 'Magnitude(Linear) ';
elseif str2double(handles.objNA.query('PHAS?;'))
    format=2;
    format_string = 'Phase';
elseif str2double(handles.objNA.query('DELA?;'))
    format=3;
    format_string = 'Delay';
elseif str2double(handles.objNA.query('SMIC?;'))
    format=4;
    format_string = 'Smith chart';
elseif str2double(handles.objNA.query('POLA?;'))
    format=5;
    format_string = 'Polar';
elseif str2double(handles.objNA.query('REAL?;'))
    format=6;
    format_string = 'Real';
elseif str2double(handles.objNA.query('SWR?;'))
    format=7;
    format_string = 'SWR';
elseif str2double(handles.objNA.query('IMAG?;'))
    format=8;
    format_string = 'Imaginary';
end


% --- Executes on button press in Connect.
function Connect_Callback(hObject, eventdata, handles)
% hObject    handle to Connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Connect.Enable='off';
drawnow

% if connected then disconnect
if handles.NA_OK==0
    handles.objNA.disconnect();
end

% Setup connection
handles=connect_instruments(handles);

[handles.NA_OK]=check_connections(handles.objNA);
% Connection Status
if(~isempty(handles.objNA.name) && handles.NA_OK==0)
    set(handles.text12, 'String', handles.objNA.name);
    handles.Capture_Refresh.Enable='on';
    drawnow
else
    set(handles.text12, 'String', 'Error Connection');
    handles.Capture_Refresh.Enable='off';
    drawnow
end

handles.Connect.Enable='on';
drawnow
% handles.objNA.clear_buffers();
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in Connection_Settings.
function Connection_Settings_Callback(hObject, eventdata, handles)
% hObject    handle to Connection_Settings (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.Connection_Settings.Enable='off';
delete([handles.settings_path handles.settings_filename])
drawnow
% Create or load setting files.
% If setting file exist then load the variables
handles=setup_instruments(handles);

handles.Connection_Settings.Enable='on';
handles.Connect.Enable='on';
drawnow
% Update handles structure
guidata(hObject, handles);

% --- Executes on button press in Export_Plot.
function Export_Plot_Callback(hObject, eventdata, handles)
% hObject    handle to Export_Plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% [FileName,PathName] = uiputfile('*.png','Save Plot as');
% saveFileName = fullfile(PathName,FileName);
% % saveas(handles.figure1,saveDataName);
% % print(saveDataName,'-dpng','-noui')
% 
% F=getframe(handles.axes1); %select axes in GUI
% figure(); %new figure
% image(F.cdata); %show selected axes in new figure
% saveas(gcf, saveFileName, 'png'); %save figure

figure()
if handles.plotformat==4 % Smith chart
    s=smithchart(handles.plotdata(:,2));
elseif handles.plotformat==5 % Polar
    % Plot polar plot
else
    plot(handles.plotdata(:,1),handles.plotdata(:,2));
    grid on;
end
pause
close(gcf); %and close it

% --- Executes on button press in Export_Data.
function Export_Data_Callback(hObject, eventdata, handles)
% hObject    handle to Export_Data (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
try
    d = handles.plotdata;
    % d = get(handles.axes1, 'Data');
    [FileName,PathName] = uiputfile('*.csv','Save Table as');
    csvwrite(fullfile(PathName, FileName),d)
catch
    errordlg('File I/O error');
end

% --- Executes on button press in btn_export_s2p.
function btn_export_s2p_Callback(hObject, eventdata, handles)
% hObject    handle to btn_export_s2p (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    % d = get(handles.axes1, 'Data');
    [FileName,PathName] = uiputfile('*.s2p','Save Values as');
    if isequal(FileName,0) || isequal(PathName,0)
        return;
    end
    fullFileName=fullfile(PathName, FileName);
    export_s2p(fullFileName, handles);
catch
    errordlg('File I/O error or 1 port calibration performed');
    % Enable the Capture button
    handles.Capture_Refresh.Enable='on';
end

function set_label(handles)
    prompt = {'Enter Xlabel','Enter Ylabel'};
    dlg_title = 'Set Labels';
    num_lines = 1;
    
    %defaultans = {handles.axes1.XLabel(1)/1E9),num2str(handles.axes1.XLim(2)/1E9),num2str(handles.axes1.YLim(1)),num2str(handles.axes1.YLim(2))};
    %answer = inputdlg(prompt,dlg_title,num_lines,defaultans);
    answer = inputdlg(prompt,dlg_title,num_lines);
    try
        if length(answer)==2
            xlabel(handles.axes1, answer{1});
            ylabel(handles.axes1, answer{2});
        end
    catch
        disp('Please enter strings')
        errordlg('Please enter strings')
    end

% --- Executes on button press in btn_set_label.
function btn_set_label_Callback(hObject, eventdata, handles)
% hObject    handle to btn_set_label (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_label(handles);

function set_axis(handles)
    unit=handles.popup_xunits.String(handles.popup_xunits.Value);
    prompt = {['Enter Xlim Min(in ' unit{1} '):'],['Enter Xlim Max(in ' unit{1} '):'],'Enter Ylim Min:','Enter Ylim Max:'};
    dlg_title = 'Set Axis';
    num_lines = 1;
    defaultans = {num2str(handles.axes1.XLim(1)),num2str(handles.axes1.XLim(2)),num2str(handles.axes1.YLim(1)),num2str(handles.axes1.YLim(2))};
    answer = inputdlg(prompt,dlg_title,num_lines,defaultans);

    try
        if length(answer)==4
            handles.axes1.XLim(1)=str2double(answer{1});
            handles.axes1.XLim(2)=str2double(answer{2});
            handles.axes1.YLim(1)=str2double(answer{3});
            handles.axes1.YLim(2)=str2double(answer{4});
        end
    catch
        disp('Please enter numbers')
        errordlg('Please enter numbers')
    end

% --- Executes on button press in btn_set_axis.
function btn_set_axis_Callback(hObject, eventdata, handles)
% hObject    handle to btn_set_axis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_axis(handles)


% --------------------------------------------------------------------
function plot_menu_Callback(hObject, eventdata, handles)
% hObject    handle to plot_menu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function menu_set_axis_Callback(hObject, eventdata, handles)
% hObject    handle to menu_set_axis (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set_axis(handles)

function check_status_reg(handles)
status_byte=handles.objNA.query('OUTPSTAT;');
esr_byte=handles.objNA.query('ESR?;');
esb_byte=handles.objNA.query('ESB?;');
h = msgbox(['Status byte:' status_byte 'ESR:' esr_byte 'ESB:' esb_byte],'NA Status');

function clear_status(handles)
handles.objNA.send('CLES;');
handles.objNA.clear_buffers();

function clear_NA_errors(handles)
errors={};

while(1)
    error=handles.objNA.query('OUTPERRO;');
    [err_num err_str]=strread(error, '%s %s', 'delimiter', ',');
    % No Errors means err_num = 0
    if(str2double(err_num)==0.0)
        break;
    end
    errors={errors{:},err_str};
end

if(isempty(errors)==0)
    err_str='';
    for i=1:length(errors)
        err_str=[err_str errors{i}];
    end
    h = msgbox(['Errors Cleared:' err_str],'Clear NA Error');
else
    h = msgbox('No NA Errors','Clear NA Error');
end

% --- Executes on button press in clear_errors_cmd.
function clear_errors_cmd_Callback(hObject, eventdata, handles)
% hObject    handle to clear_errors_cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear_NA_errors(handles)


% --- Executes on button press in status_cmd.
function status_cmd_Callback(hObject, eventdata, handles)
% hObject    handle to status_cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
check_status_reg(handles);


% --- Executes on button press in clear_status_cmd.
function clear_status_cmd_Callback(hObject, eventdata, handles)
% hObject    handle to clear_status_cmd (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clear_status(handles);


% --- Executes on button press in checkbox_hold_plot.
function checkbox_hold_plot_Callback(hObject, eventdata, handles)
% hObject    handle to checkbox_hold_plot (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkbox_hold_plot
value = get(hObject,'Value');
if value == 0
    hold(handles.axes1,'off');
else
    hold(handles.axes1,'on');
end
% Update handles structure
guidata(hObject, handles);

% --- Executes on selection change in popup_NA_channel.
function popup_NA_channel_Callback(hObject, eventdata, handles)
% hObject    handle to popup_NA_channel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_NA_channel contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_NA_channel
idx = hObject.Value;
if idx ==1
   handles.objNA.send('DUACOFF;');
   handles.objNA.send('CHAN1;');
elseif idx ==2
    handles.objNA.send('DUACOFF;');
   handles.objNA.send('CHAN2;');
elseif idx ==3
   handles.objNA.send('DUACON;');
end
% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popup_NA_channel_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_NA_channel (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_polarplot.
function popup_xunits_Callback(hObject, eventdata, handles)
% hObject    handle to popup_xunits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_xunits contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_xunits
% ticks=handles.axes1.XTick;
% ticks_label=handles.axes1.XTickLabel;

idx = hObject.Value;
if handles.domain==0
    if idx ==1
        handles.xunits=1E9;    %GHz
    elseif idx ==2
        handles.xunits=1E6;    %MHz
    end
elseif handles.domain==1
    if idx ==1
        handles.xunits=1E-9;    %nsec
    elseif idx ==2
        handles.xunits=1E-6;    %usec
    end
end
% handles.axes1.XTick=ticks;
% handles.axes1.XTickLabel={};
% for i=1:length(ticks)
%     handles.axes1.XTickLabel={handles.axes1.XTickLabel{:},num2str(ticks(i))};
% end

% Update handles structure
guidata(hObject, handles);

% --- Executes during object creation, after setting all properties.
function popup_xunits_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_xunits (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popupmenu6.
function popupmenu6_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu6 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu6


% --- Executes during object creation, after setting all properties.
function popupmenu6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_trace_style.
function popup_trace_style_Callback(hObject, eventdata, handles)
% hObject    handle to popup_trace_style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_trace_style contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_trace_style
idx = hObject.Value;
if idx ==1
   handles.trace_style='-';
elseif idx ==2
   handles.trace_style='--';
elseif idx ==3
   handles.trace_style='-.';
elseif idx ==4
   handles.trace_style=':';
end
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popup_trace_style_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_trace_style (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in popup_color.
function popup_color_Callback(hObject, eventdata, handles)
% hObject    handle to popup_color (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_color contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_color
idx = hObject.Value;
if idx ==1
   handles.trace_color='b';
elseif idx ==2
   handles.trace_color='r';
elseif idx ==3
   handles.trace_color='y';
elseif idx ==4
   handles.trace_color='m';
elseif idx ==5
   handles.trace_color='c';
elseif idx ==6
   handles.trace_color='g';
elseif idx ==7
   handles.trace_color='k';
elseif idx ==8
   handles.trace_color='w';   
end
% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popup_color_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_color (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end




% --- Executes on selection change in popup_NA_domain.
function popup_NA_domain_Callback(hObject, eventdata, handles)
% hObject    handle to popup_NA_domain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns popup_NA_domain contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popup_NA_domain

idx = hObject.Value;
if idx ==1
    handles.domain=0;    %Frequency
    handles.xunits=1E9;  % GHz
    set(handles.popup_xunits,'string',{'GHz','MHz'})
elseif idx ==2
    handles.domain=1;    %Time
    handles.xunits=1E-9;  % nsec
    set(handles.popup_xunits,'string',{'ns','us'})
end

% Update handles structure
guidata(hObject, handles);


% --- Executes during object creation, after setting all properties.
function popup_NA_domain_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popup_NA_domain (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
