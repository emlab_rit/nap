%
%   Export Touchstone file (or s2p file)
%
%

function export_s2p(filename, handles)
    % Get values from NA
    
    % Enable the Capture button
    handles.Capture_Refresh.Enable='off';

    number_of_points=str2double(handles.dev.query('POIN?;'));
    % Create a matrix (i,j,mag or phase,points)
    S=zeros(2,2,2,number_of_points);
    
    % Get S11, S21, S12 and S22
    for i=1:2
        for j=1:2
            for k=1:2
                [freq, S(i,j,k,:)]=get_data(handles,i,j,k);
            end
        end
    end
    
    % Enable the Capture button
    handles.Capture_Refresh.Enable='on';
    
    % Save values as s2p file
    % S11(linmag) and S11 (angdeg)
%     Changing S11 into complex number since sparameters works with it.
    
    S_parameters=zeros(2,2,number_of_points);
    for i=1:2
        for j=1:2
            S_parameters(i,j,:)=S(i,j,1,:).*exp(1i*S(i,j,2,:)*pi/180);
        end
    end
    
    rfwrite(S_parameters, freq, filename, 'Format','DB');
end

% Get data from NA based on measurement and format
% (meas1,meas2) = (1,1) == S11
% (meas1,meas2) = (2,1) == S21
% (meas1,meas2) = (1,2) == S12
% (meas1,meas2) = (2,2) == S22
% format = 1 -- LOGM and 2 -- PHASE
function [freq, data]=get_data(handles, meas1, meas2, format)
    try
        start_freq=str2double(handles.dev.query('STAR?;'));
        stop_freq=str2double(handles.dev.query('STOP?;'));
        number_of_points=str2double(handles.dev.query('POIN?;'));
        points=1:number_of_points;
        freq_sweep=start_freq+(points-1)*(stop_freq-start_freq)/(number_of_points-1);

        pause(0.5);

%           Required measurement (S11, S21, S12, S22)
%           Port A = 1 (MEASA), Port B = 2, RFLP and TRAP
%             if(meas1 == 1)
%                 handles.dev.send('MEASA;');
%                 pause(0.5);
%                 if(meas2 == 1)
%                     handles.dev.send('RFLP;');
%                 elseif(meas2 == 2)
%                     handles.dev.send('TRAP;');
%                 end
%             elseif(meas1 == 2)
%                 handles.dev.send('MEASB;');
%                 pause(0.5);
%                 if(meas2 == 1)
%                     handles.dev.send('TRAP;');
%                 elseif(meas2 == 2)
%                     handles.dev.send('RFLP;');
%                 end

            if(meas2 == 1)
                if(meas1 == 1)
                    handles.dev.send('S11;');
                elseif(meas1 == 2)
                    handles.dev.send('S21;');
                end
            elseif(meas2 == 2)
                if(meas1 == 1)
                    handles.dev.send('S12;');
                elseif(meas1 == 2)
                    handles.dev.send('S22;');
                end
            else
                errordlg('Error -- wrong MEAS.');
                % Enable the Capture button
                handles.Capture_Refresh.Enable='on';
                return;
            end
            
            pause(0.5);
            
%           Required format (LOGMAG, PHASE)
            if(format == 1)
                handles.dev.send('LINM;');
            elseif(format == 2)
                handles.dev.send('PHAS;');
            else
                errordlg('Error -- wrong FORMAT.');
                % Enable the Capture button
                handles.Capture_Refresh.Enable='on';
                return;
            end
            
            pause(0.5);
            
            handles.dev.send('FORM5;');
            handles.dev.send('OUTPFORM;');

            data=handles.dev.read_form5_data(number_of_points);
            if(isempty(data))
                return
            end

            % Rearranging the data
            freq=freq_sweep';
            data=data';
            data=data(:,1);
        catch
            errordlg('Error while getting data.');
            % Enable the Capture button
            handles.Capture_Refresh.Enable='on';

     end
end